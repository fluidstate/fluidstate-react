/**
 * Unfortunately Jest does not have a way to test that a rerender attempt does not happen
 * after unmount. To test it, we create test-only counter that is increased whenever rerender
 * is attempted inside our hook / HOC. This counter is excluded from the final library
 */
export const testCounter: {
	isCounting?: boolean;
	count?: number;
	startCount?: () => void;
	stopCount?: () => void;
	increaseCount?: () => void;
	// @ts-ignore
} = __DEV__
	? {
			isCounting: false,
			count: 0,
			startCount: () => {
				testCounter.count = 0;
				testCounter.isCounting = true;
			},
			stopCount: () => {
				const result = testCounter.count;
				testCounter.count = 0;
				testCounter.isCounting = false;
				return result;
			},
			increaseCount: () => {
				if (testCounter.isCounting) {
					testCounter.count =
						testCounter.count == null ? 0 : testCounter.count + 1;
				}
			},
	  }
	: {};
