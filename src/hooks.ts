import { useState, useCallback, useEffect, useRef } from "react";
import { testCounter } from "./test-counter";
import { useOnceRef } from "./once-ref";
import { createObserved, get, set } from "fluidstate/core";
import { createReaction } from "fluidstate";

export const useFluidState = <T>(
	getState: () => T,
	dependencyArray: ReadonlyArray<unknown> = []
) => {
	const isWithinRender = useRef(true);
	isWithinRender.current = true;

	// result is a holder for the value returned from getState
	const result = useRef(null as T);
	const [, rerender] = useState(Symbol());

	// Since we put the get state under dependency array, it'll change when
	// dependency array is changed
	const getStateCallback = useCallback(getState, dependencyArray);

	// An observed for getState function which will be used in the effect
	const getStateObserved = useOnceRef(() => createObserved(getStateCallback));

	const stopReaction = useOnceRef(() =>
		createReaction(
			() => get(getStateObserved.current)(),
			(value) => {
				result.current = value;
				// If the result is first initialized or happens due to dependency array change
				// during render, we do not need to rerender, but is rerendered upon other changes
				if (!isWithinRender.current) {
					rerender(Symbol());
					// @ts-ignore
					if (__DEV__ && testCounter.increaseCount) {
						testCounter.increaseCount();
					}
				}
			}
		)
	);

	// The effect is stopped when component unmounts
	useEffect(() => {
		return () => {
			stopReaction.current();
		};
	}, []);

	// Upon dependency array change, if the callback is different, it'll recalculate
	// the effect since it depends on it
	if (getStateObserved.current.value !== getStateCallback) {
		set(getStateObserved.current, getStateCallback);
		get(getStateObserved.current);
	}

	isWithinRender.current = false;

	// Returning the current value from getState since it'll be kept
	// up to date due to rerendering
	return result.current;
};
