import { describe, expect, test } from "@jest/globals";
import { withFluidState } from "../index";
import { FunctionComponent } from "react";

describe("HOC component metadata", () => {
	test("component metadata is preserved", () => {
		const Component: FunctionComponent<{}> = () => {
			return null;
		};
		Component.contextTypes = {};
		Component.propTypes = {};
		Component.defaultProps = {};
		Component.displayName = "MyComponent";
		const FluidComponent = withFluidState(Component);
		expect(Component.contextTypes === FluidComponent.contextTypes).toEqual(
			true
		);
		expect(Component.propTypes === FluidComponent.propTypes).toEqual(true);
		expect(Component.defaultProps === FluidComponent.defaultProps).toEqual(
			true
		);
		expect(FluidComponent.displayName).toEqual(`FluidComponent(MyComponent)`);
	});
});
