import React, { useState } from "react";
import { create } from "react-test-renderer";
import { describe, expect } from "@jest/globals";
import { act, dedent, wrapExample } from "./utils";
import { useFluidState } from "../index";

type FluidState = {
	name: string;
};

type Props = {
	age: number;
	state: FluidState;
};

type ReactState = {
	isMarried: boolean;
};

describe("Mixing props, Fluid State and React state", () => {
	const example = wrapExample<FluidState, Props>(
		() => ({
			name: "John",
		}),
		({ state, age }) => {
			const { name } = useFluidState(() => state);
			const [reactState, setReactState] = useState<ReactState>(() => ({
				isMarried: false,
			}));
			return (
				<div>
					Hello {name} - your age is {age} - you are{" "}
					{!reactState.isMarried ? "not" : ""} married.
					<button
						id="marriage-toggle"
						onClick={() =>
							setReactState((state) => ({
								...state,
								isMarried: !state.isMarried,
							}))
						}
					>
						Toggle marriage status
					</button>
				</div>
			);
		},
		({ state, age }) => {
			const [reactState, setReactState] = useState<ReactState>(() => ({
				isMarried: false,
			}));
			return (
				<div>
					Hello {state.name} - your age is {age} - you are{" "}
					{!reactState.isMarried ? "not" : ""} married.
					<button
						id="marriage-toggle"
						onClick={() =>
							setReactState((state) => ({
								...state,
								isMarried: !state.isMarried,
							}))
						}
					>
						Toggle marriage status
					</button>
				</div>
			);
		}
	);

	example.test(
		"can perform updates on all things",
		async (Component, state, getRenderCount) => {
			const instance = create(<Component state={state} age={35} />);
			expect(instance).toMatchInlineSnapshot(dedent`
				<div>
				  Hello 
				  John
				   - your age is 
				  35
				   - you are
				   
				  not
				   married.
				  <button
				    id="marriage-toggle"
				    onClick={[Function]}
				  >
				    Toggle marriage status
				  </button>
				</div>
			`);
			expect(getRenderCount()).toEqual(1);

			await act(() => {
				instance.update(<Component state={state} age={36} />);
			});
			expect(instance).toMatchInlineSnapshot(dedent`
				<div>
				  Hello 
				  John
				   - your age is 
				  36
				   - you are
				   
				  not
				   married.
				  <button
				    id="marriage-toggle"
				    onClick={[Function]}
				  >
				    Toggle marriage status
				  </button>
				</div>
			`);
			expect(getRenderCount()).toEqual(2);

			await act(() => {
				instance.root.findByProps({ id: "marriage-toggle" }).props.onClick();
			});
			expect(instance).toMatchInlineSnapshot(dedent`
				<div>
				  Hello 
				  John
				   - your age is 
				  36
				   - you are
				   
				   married.
				  <button
				    id="marriage-toggle"
				    onClick={[Function]}
				  >
				    Toggle marriage status
				  </button>
				</div>
			`);
			expect(getRenderCount()).toEqual(3);

			await act(() => {
				state.name = "Mary";
			});
			expect(instance).toMatchInlineSnapshot(dedent`
				<div>
				  Hello 
				  Mary
				   - your age is 
				  36
				   - you are
				   
				   married.
				  <button
				    id="marriage-toggle"
				    onClick={[Function]}
				  >
				    Toggle marriage status
				  </button>
				</div>
			`);
			expect(getRenderCount()).toEqual(4);
		}
	);
});
