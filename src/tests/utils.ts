import oDedent from "ts-dedent";
import { act as testAct } from "react-test-renderer";
import { FunctionComponent } from "react";
import { test } from "@jest/globals";
import { createReactive } from "fluidstate";
import { withFluidState } from "../index";

const normalizeSpace = (value: string) => value.replace(/\t/g, "  ");

export const dedent = (
	staticParts: TemplateStringsArray | string,
	...dynamicParts: unknown[]
) => {
	return "\n" + normalizeSpace(oDedent(staticParts, ...dynamicParts)) + "\n";
};

export const tick = () => {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(0);
		}, 0);
	});
};

export const act = async (callback: () => void | Promise<void>) => {
	await testAct(async () => {
		await callback();
		await tick();
	});
};

const getRenderCountData = () => {
	let count = 0;
	const increaseRenderCount = () => {
		count++;
	};
	const getRenderCount = () => {
		return count;
	};
	return { increaseRenderCount, getRenderCount };
};

export const wrapExample = <T, P extends { state: T }>(
	initState: () => T,
	hookComponent: FunctionComponent<P>,
	hocComponent: FunctionComponent<P>
) => {
	return {
		test: (
			name: string,
			testExample: (
				Component: FunctionComponent<P>,
				state: T,
				getRenderCount: () => number
			) => void | Promise<void>
		) => {
			test(`${name} - hook component`, () => {
				const state = createReactive(initState());
				const { increaseRenderCount, getRenderCount } = getRenderCountData();
				return testExample(
					(...args) => {
						increaseRenderCount();
						return hookComponent(...args);
					},
					state,
					getRenderCount
				);
			});

			test(`${name} - HOC component`, () => {
				const state = createReactive(initState());
				const { increaseRenderCount, getRenderCount } = getRenderCountData();
				return testExample(
					withFluidState((...args) => {
						increaseRenderCount();
						return hocComponent(...args);
					}),
					state,
					getRenderCount
				);
			});
		},
	};
};
