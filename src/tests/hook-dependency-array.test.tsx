import React, { useState } from "react";
import { create } from "react-test-renderer";
import { describe, expect, test } from "@jest/globals";
import { useFluidState } from "../index";
import { createReactive } from "fluidstate";
import { act, dedent } from "./utils";

type FluidState = {
	spouseName: string;
};

describe("Hook dependency array", () => {
	test("hook dependency array works", async () => {
		let renderCount = 0;
		const state = createReactive<FluidState>({
			spouseName: "John",
		});
		const Component = ({ state }: { state: FluidState }) => {
			renderCount++;
			const [isMarried, setMarried] = useState(false);
			const value = useFluidState(
				() => (isMarried ? `Spouse name: ${state.spouseName}` : "Not married"),
				[isMarried]
			);
			return (
				<div>
					{value}
					<br />
					<button onClick={() => setMarried(!isMarried)}>Toggle</button>
				</div>
			);
		};

		const instance = create(<Component state={state} />);
		expect(instance).toMatchInlineSnapshot(dedent`
			<div>
			  Not married
			  <br />
			  <button
			    onClick={[Function]}
			  >
			    Toggle
			  </button>
			</div>
		`);
		expect(renderCount).toEqual(1);

		await act(() => {
			state.spouseName = "Anatoly";
		});
		expect(instance).toMatchInlineSnapshot(dedent`
			<div>
			  Not married
			  <br />
			  <button
			    onClick={[Function]}
			  >
			    Toggle
			  </button>
			</div>
		`);
		expect(renderCount).toEqual(1);

		await act(() => {
			instance.root.findByType("button").props.onClick();
		});
		expect(instance).toMatchInlineSnapshot(dedent`
			<div>
			  Spouse name: Anatoly
			  <br />
			  <button
			    onClick={[Function]}
			  >
			    Toggle
			  </button>
			</div>
		`);
		expect(renderCount).toEqual(2);

		await act(() => {
			state.spouseName = "Joan";
		});
		expect(instance).toMatchInlineSnapshot(dedent`
			<div>
			  Spouse name: Joan
			  <br />
			  <button
			    onClick={[Function]}
			  >
			    Toggle
			  </button>
			</div>
		`);
		expect(renderCount).toEqual(3);

		await act(() => {
			instance.root.findByType("button").props.onClick();
		});
		expect(instance).toMatchInlineSnapshot(dedent`
			<div>
			  Not married
			  <br />
			  <button
			    onClick={[Function]}
			  >
			    Toggle
			  </button>
			</div>
		`);
		expect(renderCount).toEqual(4);

		await act(() => {
			state.spouseName = "Brian";
		});
		expect(instance).toMatchInlineSnapshot(dedent`
			<div>
			  Not married
			  <br />
			  <button
			    onClick={[Function]}
			  >
			    Toggle
			  </button>
			</div>
		`);
		expect(renderCount).toEqual(4);
	});
});
