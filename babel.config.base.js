module.exports.createBaseBabelConfig = (isDev) => ({
	presets: [
		["@babel/preset-env", { targets: { node: "current" } }],
		"@babel/preset-typescript",
		"@babel/preset-react",
	],
	plugins: [
		[
			"minify-replace",
			{
				replacements: [
					{
						identifierName: "__DEV__",
						replacement: {
							type: "numericLiteral",
							value: isDev ? 1 : 0,
						},
					},
				],
			},
		],
	],
	sourceMaps: true,
});
